    methods (access=restricted)
        function stepForwardFunc
            imsiz = size(IM);
            % makes sure the imput images match the size defined by the layer
            assert(all(imsiz(1:length(self.imsize_in)) == self.imsize_in));
            nim = imsiz(end);
            imsize_in = self.imsize_in;
            imsize_out = self.imsize_out;
            % pad the input pictures if necessary
            pad = ceil( imsize_in(1:length(self.psize))./self.stride).*self.psize - imsize_in(1:length(self.psize));
            for m=1:length(pad)
                if pad(m)==0, continue; end
                IM = cat(m,   zeros([1 , imsize_in(2:end)]), IM);
            end
            % init output and index storage
            IM_    = zeros([imsize_out nim]);
            IND_MP = zeros([imsize_out nim]);
            [d2,d1] = meshgrid( [0:(imsize_out(2)-1)].*self.stride(2)+1, [0:(imsize_out(1)-1)].*self.stride(1)+1 );
            % max pool - vectorized
            for m=1:imsize_out(1)
                for n = 1:imsize_out(2)
                    i1 = d1(m,n)+[0:self.psize(1)-1];
                    i2 = d2(m,n)+[0:self.psize(2)-1];
                    [IM_(m,n,:,:) , IND_MP(m,n,:,:)] = max( reshape(IM(i1,i2,:), [ prod(self.psize) imsize_in(end) nim]) ,[],1);
                end
            end
        end
    end