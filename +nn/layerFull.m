classdef layerFull < nn.layerInterface
    
    properties
        % inherited
        type     % types : Full, Conv, Pool, ( Pruned ?, Relu ?, Recursive ?)
        nin      % number of imput units
        nout     % number of output units
        wsize    % size of weight array
        woffset  % 0-based offset of the first weight in the full network weight vector
        NET      % network attached
        ilayer   % index of the layer within the network (from base)
        activationFcn     = @sigmoid
        activationFcnDiff = @sigmoidGradient
        contiguousSamples = true
        % others
        % ...
    end
    
    methods
        %% Constructor
        function self = layerFull(nin,nout,varargin)
            % self = layerFull(nin,nout)
            self.type = 'Full';
            self.wsize = [nout,nin+1 ];
            self.nin = nin;
            self.nout = nout;
        end
        %% Forward Propagation
        function [A,Z]  = stepForward (self,IN,varargin)
            % [A,Z]  = stepForward(IN);
            % forward propagation step
            % A is the output, Z is the output without the activation function applied
%             siz = size(IN); IN = reshape(IN, [prod(siz(1:length(siz)-1)) siz(end)]);
            IN = nn.squeeze2D(IN);
            Z = self.W * [IN(1,:).*0+1 ;  IN(:,:)] ;
            A = self.activationFcn(Z);
        end
        %% Backward Propagation
        function  [delta] = stepBackward(self,Z,delta)
            % [delta] = stepBackward(self,Z,delta)
            Z = nn.squeeze2D(Z);
            m = size(delta,2);
            % this is for the first layer
            if nargout==0 
                self.setDELTA( transpose( 1./m .*  [Z(1,:).*0+1 ; (Z)] *delta.' ));
                return
            end
            % otherwise for any higher layer
            self.setDELTA( transpose ( 1./m .* [Z(1,:).*0+1 ; self.activationFcn(Z)] * delta'));
            W = self.W;
            delta = ( W(:,2:end).' * delta) .* self.activationFcnDiff(Z);
        end
    end
    
end
% instance memory : 8*(nin+1)*nout*2

% ```
% a{1}=[m,400]        a{2}=[m,25]             a{3}=[m,10]
%         Theta{1}=[25,401]       Theta{2}=[10,26]
%                     d{2}=[m,25]             d{3}=[m,10]
%         DELTA{1}=[25,401]       DELTA{1}=[10,26]
% Z{1}=[m,400]        Z{2}=[m,25]             Z{3}=[m,10]
% ```