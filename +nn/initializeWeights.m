function W = initalizeWeights(siz, eps_init)
% W = initalizeWeights([nx ny],eps_init);

if nargin <=1 , eps_init = sqrt(6)./sqrt(sum(siz)); end
W = rand(siz).*2.*eps_init - eps_init;