classdef (Abstract) layerInterface < handle
   
    
    properties (Abstract)
        type     % types : Full, Conv, Pool, ( Pruned ?, Relu ?, Recursive ?)
        nin      % number of imput units (scalar)
        nout     % number of output units (scalar)
        wsize    % size of weight array
        woffset  % 0-based offset of the first weight in the full network weight vector
        NET      % network attached
        ilayer   % index of the layer within the network (from base)
        contiguousSamples % order in memory (true for full connect, false for convnets for optimal operations)
        activationFcn
        activationFcnDiff
    end
    
    methods (Abstract)
        Y    = stepForward (self,X)
        grad = stepBackward(self,Z,delta)
    end
    
     %% Getters & Setters
    methods   
        function w = W(self)
            w = self.NET.getW(self.ilayer);
        end
        function d = DELTA(self)
            d = self.NET.getDELTA(self.ilayer);
        end
        function setW(self,W)
            self.NET.setW(W,self.ilayer);
        end
        function setDELTA(self,DELTA)
            self.NET.setDELTA(DELTA, self.ilayer);
        end
        
    end
end