function C = bigConv(A,w)
%% A*w = B;
% todo (0) handle memory versus speed for big matrices
% todo (1) add stride
% todo (2) more than 2 dimensions

sza = size(A);
szw = size(w);
szc = sza - szw +1;

[x,y] =  meshgrid(1:szw(1) , 0:szc(1)-1);
[X,Y] = meshgrid([0:szw(2)-1].*sza(1) , [0:szc(2)-1].*sza(1));
C = A( repmat(x+y , [szc(2) szw(2)]) +   kron(X+Y, x.*0+1)) * flipud(w( :));

C = reshape(C,szc);
