classdef layerPoolInterface < nn.layerInterface
    
    properties
        % inherited
        type     % types : Full, Conv, Pool, ( Pruned ?, Relu ?, Recursive ?)
        nin      % number of imput units (scalar)
        nout     % number of output units (scalar)
        wsize    % size of weight array
        woffset  % 0-based offset of the first weight in the full network weight vector
        NET      % network attached
        ilayer   % index of the layer within the network (from base)
        activationFcn
        activationFcnDiff
        % original
        imsize_in
        imsize_out
        psize % size of pooling matrix
        stride % stride
        fcn    % pooling function
        contiguousSamples = false
    end
    methods
        %% Constructor
        function self = layerPoolInterface(imsize,psize,stride)
            % self = layerPool(nin,psize,[stride])
            % nin : size of the input (scalar)
            % psize : pooling size [3 2]
            % stride (optional) : pooling size by default
            if nargin <= 2, stride = psize; end
            self.type = 'Pool';
            self.stride = [stride(:) ; ones(length(imsize)-length(stride),1)]';
            self.psize =  [psize(:)  ; ones(length(imsize)-length(psize) ,1)]';
            assert(all(self.psize >= self.stride)); % the pooling size should be greatereq than stride
            self.imsize_in = imsize(:)';
            self.imsize_out = (self.imsize_in + self.stride - self.psize)./self.stride;
            % makes sure the output size is an integer number
            assert(all(mod(self.imsize_out,1)==0)); %TODO make it work with all sizes or at least catch error
            self.fcn = @max; % could be @median also ??
            self.nin = prod(self.imsize_in);
            self.nout= prod(self.imsize_out);
        end
    end    
end

