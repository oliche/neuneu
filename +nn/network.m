classdef network < handle
    properties
        nlayers
        layers
        isTrained = 0;
        lambda = 0;
        W = [];     % neuron weights
        DELTA = []; % neuron gradients
    end
    
    methods
        %% Constructor
        function self = network(layers)
            % self = network(layers)
            % todo add the epsilon for initializaion as a parameter of the constructor
            % todo add the regularization parameter lambda as a parameter of the construnctor
            % layers is a vector of layers
            self.layers = layers(:);
            self.nlayers = length(layers);
            % compute total size
            nW = 0;
            for n = 1: self.nlayers,
                layers{n}.woffset = nW;
                nW = nW + prod(layers{n}.wsize);
                % attach the network object to each layer
                layers{n}.NET  = self;
                layers{n}.ilayer = n;
            end
            % init weights and gradients in a big vector to avoid memory duplication
            self.W = nn.initializeWeights([nW,1]);
            self.DELTA = zeros(nW,1);
        end
        %% Getters / Setters for weights and gradients
        function w = getW(self, nlayer)
            w = self.W([1:prod(self.layers{nlayer}.wsize)] + self.layers{nlayer}.woffset);
            w = reshape(w, self.layers{nlayer}.wsize );
        end
        function setW(self, W, nlayer)
            self.W([1:prod(self.layers{nlayer}.wsize)] + self.layers{nlayer}.woffset) = W(:);
        end
        function d = getDELTA(self, nlayer)
            d = self.DELTA([1:prod(self.layers{nlayer}.wsize)] + self.layers{nlayer}.woffset);
            d = reshape(d, self.layers{nlayer}.wsize );
        end
        function setDELTA(self, DELTA, nlayer)
            self.DELTA([1:prod(self.layers{nlayer}.wsize)] + self.layers{nlayer}.woffset) = DELTA(:);
        end
        %% Straight Prediction
        function [y,X] = predict(self,X);
            % IN = predict(self,IN);
            % Forward propagation
            for l = 1:self.nlayers
                X = self.layers{l}.stepForward(X);
            end
            % shrink prediction into a class index
            [~,y] = max(X,[],1);
        end
        %% Feed-forward and back-propagation
        function [X,Z] = feedForward(self,X);
            % [Z,OUT] = feedForward(self,IN);
            Z=cell(self.nlayers,1);
            for l = 1:self.nlayers
                [X,Z{l}] = self.layers{l}.stepForward(X);
            end
        end
        function feedBackward(self,X,Y,Z);
            % feedBackward(self,X,Y,Z);
                delta =  self.layers{end}.activationFcn(Z{self.nlayers})-Y ;
            for l = self.nlayers:-1:1
                if l==1, self.layers{l}.stepBackward(X,     delta); continue,  end 
                 delta = self.layers{l}.stepBackward(Z{l-1},delta);
            end
        end
        
        function [J,grad]= computeCost(self,X,Y);
            % feed forward to get a prediction
            backprop = false; if nargout == 2, backprop = true; end
            if backprop % if backpropagation is required, keep intermediate states in memory
                [OUT,Z] = self.feedForward(X);
            else % otherwise perform a straight prediction
                OUT = self.feedForward(X);
            end
            [n,m] = size(OUT);
            % deflate the labels if necessary
            if size(Y,1)==1, Y = double(repmat(Y,n,1) == repmat( [1:n]',1,m)); end
            % compute the cost
            J = 1./m .* sum(flatten( - ( Y.*log(OUT+eps) + (1-Y).*log(1-OUT+eps)))); % compute the cost
            % add regularization term to the cost
            if self.lambda % no need to compute if not required
                reg_term = 0;
                for l=1:self.nlayers
                    W = self.getW(l);
                    reg_term = reg_term + sum( flatten(W(:,2:end).^2 ));
                end
                J = J + reg_term .* self.lambda./2./m;
            end
            % exit now if no gradient computation
            if ~backprop, return, end
            feedBackward(self,X,Y,Z);
            grad = true;
        end
        % wrapper for optimization functions
        function [J,grad] = trainFcn(self,W,X,y)
            self.W = W ;
            [J,flag] = self.computeCost(X,y);
            grad = self.DELTA;
        end
    end
end