classdef layerPoolMean < nn.layerPoolInterface
        
    methods
        %% Constructor
        function self = layerPoolMean(varargin)
            % Call super class constructor
            self@nn.layerPoolInterface(varargin{:});
        end
        %% Forward Prop
        function [IM_,IND_MP]  = stepForward (self,IM)
            % [A]  = stepForward(IM);
            % IM is a 5D array, images [ nx , ny , nz , nd , nim]
            % forward propagation step
            IND_MP = []; % there is nothing to keep here
            psize = self.psize;
            imsize = size(IM);
            decim = @(dim) [ floor((psize(dim)+1)/2) : psize(dim) : (imsize(dim)-ceil((psize(dim)  )/2)+psize(dim)-1) ];
            filter_ = ones(psize)./prod(psize);
            IM_ = convn( IM , filter_ ,'same');
            IM_ = IM_(decim(1),decim(2),:,:);
        end
        %% Back Prop
        function delta = stepBackward(self,Z,delta)
            % NB use kron function here
            % psize
            % stride
            assert(self.stride == self.psize)
            opup = zeros(self.psize);
            delta =  kron(opup, delta);
        end
    end
end