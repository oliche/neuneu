function X = squeeze2D(X)
% X = nn.squeeze2D(X)
% reshapes a ndmatrix by putting all dimensions but the last together
% A([7,7,100,90]) will result in a [4900, 90] matrix
% 

nd = ndims(X);
if nd < 3, return, end
sz = size(X);
X = reshape(X, prod(sz(1:end-1)) , sz(end) );