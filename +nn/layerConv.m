classdef layerConv < nn.layerInterface
    
    properties
        % inherited
        type     % types : Full, Conv, Pool, ( Pruned ?, Relu ?, Recursive ?)
        nin      % number of imput units (scalar)
        nout     % number of output units (scalar)
        wsize    % size of weight array
        woffset  % 0-based offset of the first weight in the full network weight vector
        NET      % network attached
        ilayer   % index of the layer within the network (from base)
        activationFcn     = @sigmoid
        activationFcnDiff = @sigmoidGradient
        contiguousSamples = false
        % specific
        DEPTH  % output volume depth
        cfsize % size of the convolution filter
        stride % stride
        imsize_in % size of the input image
        imsize_out % size of the output image
    end
    
    methods
        %% Constructor
        function self = layerConv(imsize, cfsize, depth, stride)
            % self = layerConv(imsize, cfsize,depth,[stride])
            % cfsize : 3 values convfilter size [5 5]
            % stride : 3 values stride [1 1]
            % depth : output volume depth
            self.nin = prod(imsize);
            self.type = 'Conv';
            if nargin <=4, stride = cfsize.*0+1; end % default is [1 1]
            self.stride = stride(:)';
            self.DEPTH = depth;
            self.cfsize = cfsize(:)'; %2D filter size in samples
            self.wsize = [prod(self.cfsize)+1 self.DEPTH]; % full size of weight array : conv filters and bias
            self.imsize_in = imsize(:)';
            self.imsize_out = [ imsize(:)'-cfsize(:)'+1  depth];
            self.nout = prod(self.imsize_out);
        end
        function [X, Z]  = stepForward(self,IM)
            %             % [A]  = stepForward(IM);
            %             % IM is a ND array, images [ nx , ny , ... , nd , nim]
            % This is the forward convolution pass. This should work for ND array as well as images
            Z = self.ForwardConvolution(IM,self.W);
            X = self.activationFcn(Z);
        end
        %% Backward Propagation
        function delta = stepBackward(self,Z,delta)
            % if it comes from a full connect, the input has to be reshaped to an image
            sz = size(delta); m = sz(end);
            delta  = reshape(delta, [self.imsize_out m]);
            
%            OUT = Convolution(self, delta,self.W)

            % Compute the gradient and store in the layer object
            nd = ndims(delta);
            Z = permute(Z , [ 1:[nd-2] nd nd-1]);
            if nargout==0  % this is for the first layer
                
                conv2(  Z(:,:,1,1) , delta(:,:,1,1),'valid')
                
                
                dd = delta(:,:,1:2,1);
                filter2(Z(:,:,1) , dd(:,:),'valid');
                
                
                
                
                size(delta)
                aa = conv2( Z(:,:,1,1:10) , delta(:,:,1,1:10), 'same');
                sum(aa ,nd)
                figure,imagesc(sum(aa ,nd))
                
                return
            end
%             self.setDELTA( transpose ( 1./m .* [Z(1,:).*0+1 ; self.activationFcn(Z)] * delta'));
    
            W = self.W; % bias = W(end,:);
            W = reshape(W(1:end-1,:),[self.cfsize self.DEPTH]);            
            % Back propagate the residuals to the layer below
            IM = convn(W(:,:,1), delta(:,:,1,:),'full');
            for dep = 2 : self.DEPTH
                IM = IM + convn(W(:,:,dep), delta(:,:,dep,:),'full');
            end
            delta = squeeze(squeeze(IM).* self.activationFcnDiff(Z));

            
            
        end
    end
    methods Access = private
        function Z = ForwardConvolution(self, IM,W)
            [imsize] = size(IM); % images have a size(nx,ny,nz,..., nimages)
            nd = ndims(IM);
            nim = imsize(end); imsize = imsize(1:end-1);
            convDim = imsize - self.cfsize + 1; % nb this convolution keeps only the valid points; edges are discarded
            Z = zeros([convDim  self.DEPTH  nim]);
            % get the weights and format them for convolution
            b = W(end,:);
            W = reshape(W(1:end-1,:),[self.cfsize self.DEPTH]);
            batch = 500; % Todo compute the size according to memory
            for ff = 1 : self.DEPTH
                c = 0;
                while c < nim
                    % compute the current batch indices
                    icb = [1:batch]+c;
                    if c+batch > nim, icb = icb(icb<=nim); end
                    % deduce the size of the images to process
                    bsize = [ imsize , length(icb) ];
                    ind_im = sub2ind( [imsize(2) nim] , [1 bsize(2)] , icb([1 end]));
                    decim = @(dim) [ floor((self.cfsize(dim)+1)/2) : 1 : (bsize(dim)-floor((self.cfsize(dim)  )/2)) ];
                    % apply convolution
                    a = filter2(W(:,:,ff), IM(:,ind_im(1):ind_im(2)),'same');
                    a = reshape(a,[bsize(1:2) length(icb)]);
                    % apply bias
                    Z(:,:,ff,icb) = squeeze(a(decim(1),decim(2),:)) + b(ff);
                    c = c+batch;
                end
            end
        end
        
    end
end




