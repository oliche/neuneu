function z = sigmoidGradient(z)
% dz = sigmoidGradient(z);

z = sigmoid(z).*(1 - sigmoid(z));
