function res = run
%res = learn_tests.run

suite = matlab.unittest.TestSuite.fromPackage('learn_tests');
res   = run(suite) ;