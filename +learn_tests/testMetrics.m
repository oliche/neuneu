classdef testMetrics < matlab.unittest.TestCase
    
    methods (Test)
        function test01(testCase)
            %% Took from the SEG facies prediction using sckikit learn
            % confusion matrix
            cm = [0 0 0 0 0 0 0 0 0;14 85 19 0 0 0 0 0 0;2 54 67 0 0 0 0 0 0;0 0 0 12 1 2 0 2 1;0 0 1 6 0 49 0 7 0;0 0 0 3 0 43 2 11 4;0 0 0 0 0 0 0 5 0;0 0 0 1 0 7 0 56 5;0 0 0 0 0 0 0 3 9];
            % compute model performance metrics
            [RES,res] = learn.metrics(cm);
            % test the results for individual classes
            precision = [  0.00  0.61  0.77  0.55  0.00  0.43  0.00  0.67  0.47  ];
            recall = [0.00  0.72  0.54  0.67  0.00  0.68  0.00  0.81  0.75  ];
            f1 = [ 0.00  0.66  0.64  0.60  0.00  0.52  0.00  0.73  0.58  ];
            assert( all( abs(res.precision - precision) < 0.009) );
            assert( all( abs(res.recall    - recall   ) < 0.009) );
            assert( all( abs(res.f1        - f1       ) < 0.009) );
            % test the overall results
            Precision =  0.54;
            Recall    =  0.58;
            F1        =  0.55;
            Accuracy  =  0.577495;
            assert(  abs(Precision - RES.Precision) < 0.009 )
            assert(  abs(F1        - RES.F1       ) < 0.009 )
            assert(  abs(Recall    - RES.Recall   ) < 0.009 )
            assert(  abs(Accuracy  - RES.Accuracy ) < 0.009 )
        end
    end
    
end
