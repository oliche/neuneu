function cm = confusionMatrix(ytrue,ypred,nclass)
% cm = learn.confusionMatrix(ytrue,ypred)
% cm = learn.confusionMatrix(ytrue,ypred,nclass)

% Get the number of classes if necessary
if nargin ==2,
    nclass = length(unique( [ytrue(:) ; ypred(:)]));
end

% Compute confusion Matrix
nech = length(ypred);
ind = sub2ind([nclass nclass], ytrue,ypred);
cm = accumarray(ind,1,[nclass*nclass 1]) ;
cm = reshape(cm, nclass, nclass);



