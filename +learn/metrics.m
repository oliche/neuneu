function [RES,res] = metrics(ytrue,ypred)
% [RES,res] = learn.metrics(confusionMatrix);
% [RES,res] = learn.metrics(ytrue,ypred);

if nargin==1 & ndims(ytrue)
    cm = ytrue;
else
    cm = learn.confusionMatrix(ytrue,ypred);
end

ns = sum(cm,2);  % number of samples for each class (true samples)
N = sum(ns); % total number of samples

%% Compute class by class results
tp = diag(cm);          % true  positives for each class
fn = sum(cm,2) - tp;    % false negatives for each class
fp = sum(cm,1).' - tp;  % false positives for each class
tn = N - tp -fn -fp; % false positives for each class
precision = tp ./ (tp + fp + eps); % precision for each class
recall    = tp ./ (tp + fn + eps); % recall for each class
f1 = 2.*tp ./ (2.*tp + fp + fn + eps); %f1 score for each class
res = struct('tp',tp,'fn',fn,'fp',fp,'tn',tn ,'precision',precision,'recall',recall,'f1',f1); % encapsulate
res = structfun(@(x) x.' , res, 'UniformOutput', false); % easier to display this way

%% Compute overall score
weights = ns.'./N; % weights for total value computations
Precision = weights*precision; % Overall Precsion
Recall = weights*recall; % Total Recall ;-)
F1 = weights*f1; % Overall F1 (weighted)
Accuracy = sum(tp)/N ; % Overall Accuracy
RES = struct('F1',F1,'Precision',Precision,'Recall',Recall,'Accuracy',Accuracy); % encapsulate


