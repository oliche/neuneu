function pca(X,k)
% learn.pca(X,k)

% a PCA algorithm can be implemented in 3 lines :
% [U,S,V]  = svd(cov(X));
% U_ = U(:,1:k);
% Z = X * U_;
% However here we take the Bayesian approach to model prior information on
% the data (nothing on the model space). This allows among other to handle
% missing data cases.

[U,S,V]  = svd(cov(X));

if nargin == 1, k = 0.8; end % threshold at 80% defaults

if k < 1 % threshold technique of the diagonal of S (eigenvalues)
    thresh = 0.8;
    ev = cumsum(diag(S) ./ sum(diag(S)));
    k = find( ev>thresh ,1,'first');
end

U_ = U(:,1:k);
Z = X * U_;

