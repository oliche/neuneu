%% Load the MNIST dataset
E:\2016\vrac\fbpick\stanford_dl_ex-master\common
chem = [fileparts(which('loadMNISTImages')) filesep];
im = loadMNISTImages([chem 'train-images-idx3-ubyte']);
im = loadMNISTImages([chem  't10k-images-idx3-ubyte']);

lab = loadMNISTLabels([chem 'train-labels-idx1-ubyte']);
lab = loadMNISTLabels([chem  't10k-labels-idx1-ubyte']);

%% Variable names and sizes for Full COnncect
% ```
% a{1}=[m,400]        a{2}=[m,25]             a{3}=[m,10]
%         Theta{1}=[25,401]       Theta{2}=[10,26]
%                     d{2}=[m,25]             d{3}=[m,10]
%         DELTA{1}=[25,401]       DELTA{1}=[10,26]
% Z{1}=[m,400]        Z{2}=[m,25]             Z{3}=[m,10]
% ```

%% Coursera neural network exercise 4 : test Full Connect Layer
run( nn_tests.testFullConnect);
run( nn_tests.testConv);

%% Try the convolution network from 
chem = [fileparts(which('loadMNISTImages')) filesep];
im = loadMNISTImages([chem 'train-images-idx3-ubyte']);
im = reshape(im,[28 28 size(im,2)]);
lab = loadMNISTLabels([chem 'train-labels-idx1-ubyte']);

L{1} = nn.layerConv([28 28],[8 8],20);
L{2} = nn.layerPoolMean(L{1}.imsize_out,[3 3]); % todo compute nout, check pooling
L{3} = nn.layerFull(L{2}.nout,10);
NET = nn.network(L);

% train network
nim = 1000;
options = optimset('MaxIter', 50);
[nn_params, cost] = nn.fmincg(  @(w) NET.trainFcn(w,im(:,:,1:nim),lab(1:nim)) , NET.W, options);
% http://ufldl.stanford.edu/tutorial/supervised/ExerciseConvolutionalNeuralNetwork/
% NET = nn.network(L);
% NET.setW(Theta1,1);
% NET.setW(Theta2,2);

%%
A = magic(3);
w = magic(2);
B = conv2(A,w,'valid');
res = abs(B - nn.bigConv(A,w)) ;
assert( all(res(:) < 4.*eps) )


A = magic(12);
w = magic(5);
B = conv2(A,w,'valid');
res = abs(B - nn.bigConv(A,w)) ;
assert( all(res(:) < 4.*eps) )

%%
sza = size(A);
szw = size(w);
[x, y] = meshgrid(1:szw(2),0:(szw(1)-1));
B = A(repmat(x+y, [nc nc]) + kron((x+y-1).*3, ones(nc)))*w([prod(szw):-1:1]');
B = reshape(B,szw)

