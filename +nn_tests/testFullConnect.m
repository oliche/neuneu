classdef testFullConnect < matlab.unittest.TestCase
    
    methods (Test)
        function testForwardAndBackProp(testCase)
            chem = [ fileparts(which('nn_tests.testFullConnect')) filesep];
            load([chem 'ex4data1.mat'])
            load([chem 'ex4weights.mat'])
            X = transpose(X); y = transpose(y);
            % ```
            % a{1}=[m,400]        a{2}=[m,25]             a{3}=[m,10]
            %         Theta{1}=[25,401]       Theta{2}=[10,26]
            %                     d{2}=[m,25]             d{3}=[m,10]
            %         DELTA{1}=[25,401]       DELTA{1}=[10,26]
            % Z{1}=[m,400]        Z{2}=[m,25]             Z{3}=[m,10]
            % ```
            L{1} = nn.layerFull( 400       , 25);
            L{2} = nn.layerFull( L{1}.nout , 10);
            NET = nn.network(L);
            NET.setW(Theta1,1);
            NET.setW(Theta2,2);
            % test 1 : compute cost, no gradient, no reg
            NET.lambda = 0;
            J = NET.computeCost(X,y); assert( abs(J-0.287629) < 1e-6 )
            disp('Cost no reg OK')
            % test 2 : compute cost, no gradient, w/ reg
            NET.lambda = 1;
            J = NET.computeCost(X,y); assert( abs(J-0.383770) < 1e-6 )
            NET.lambda = 3;
            J = NET.computeCost(X,y); assert( abs(J-0.576051) < 1e-6 )
            disp('Cost w/ reg OK')
            % test 3 : backprop no reg, checking the gradients
            NET.lambda = 0;
            [J,grad] = NET.computeCost(X,y);
            % this is the part that checks the gradient
            disp('Compute numerical gradients...')
            e = 1e-4;
            numgrad = NET.DELTA.*0;
            inp = sort(ceil(rand(100,1) .* length(NET.W)));
            for np = inp(:)'
                % un coup � gauche
                NET.W(np) = NET.W(np) -e;
                loss1 = NET.computeCost(X,y);
                % un coup � droite
                NET.W(np) = NET.W(np) + 2.*e;
                loss2 = NET.computeCost(X,y);
                % on regarde au milieu
                numgrad(np) = (loss2 - loss1) / (2*e);
                NET.W(np) = NET.W(np) -e;
            end
            assert(all(abs(numgrad(inp) - NET.DELTA(inp))<1e8))
            disp('Gradients OK')
        end
        
        function testTraining(testCase)
            % test 4 : at least 90% prediction
            chem = [ fileparts(which('nn_tests.testFullConnect')) filesep];
            load([chem 'ex4data1.mat']); X = transpose(X); y = transpose(y);
            % create network
            L{1} = nn.layerFull(400,25);
            L{2} = nn.layerFull( 25,10);
            NET = nn.network(L);
            % train network
            options = optimset('MaxIter', 50);
            [nn_params, cost] = nn.fmincg(  @(w) NET.trainFcn(w,X,y) , NET.W, options);
            % predict
            ypred = NET.predict(X);
            accuracy =  (1 - sum(y~=ypred) / length(y)) *100;
            disp(['Prediction accuracy : ' num2str(accuracy) '%' ])
            assert(accuracy > 90)
        end
    end
    
end