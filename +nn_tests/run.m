function res = run
%res = nn_tests.run

suite = matlab.unittest.TestSuite.fromPackage('nn_tests');
res   = run(suite) ;