classdef testbigConv < matlab.unittest.TestCase
    
    methods (Test)
        function test2D(testCase)
            
            A = magic(5);
            w = magic(3);
            B = conv2(A,w,'valid');
            res = abs(B - nn.bigConv(A,w)) ;
            assert( all(res(:) < 4.*eps) )
            
            A = rand(5,3);
            w = magic(2);
            B = conv2(A,w,'valid');
            res = abs(B - nn.bigConv(A,w)) ;
            assert( all(res(:) < 4.*eps) )
        end
        
    end
    
    
end


