%% Code used to create the test data
% im = convImages; imconv = convolvedFeatures; impool = pooledFeatures;
% w = [ reshape(W,64,100); b.'];
% chem = fileparts(which('nn_tests.testconv'));
% matfile = [chem filesep 'testConv.mat'];
% save(matfile,'im','imconv','impool','w');

classdef testConv < matlab.unittest.TestCase
    
    methods (Test)
        function testForwardProp(testCase)
            %% 1) Test Forward Propagation
            clear
            chem = fileparts(which('nn_tests.testConv'));
            matfile = [chem filesep 'testConv.mat'];
            load(matfile)
            sz = size(im);
            L{1} = nn.layerConv(sz(1:2),[8 8],100);
            L{2} = nn.layerPoolMean(L{1}.imsize_out,[3 3]); % todo compute nout, check pooling
            L{3} = nn.layerFull(L{2}.nout,3,10);
            NET = nn.network(L);
            
            L{1}.setW(w);
            [X,Z] = L{1}.stepForward(im(:,:,1:8)); % X [21 21 100 8]
            % this is testing the forward convolution part of the network
            assert(all(X(:)==imconv(:)))
            disp('Forward convolution is fine !')
            % this is testing the pooling part of the network
            [X2,Z] = L{2}.stepForward(X); % X2 [7 7 100 8] 
            assert(all( abs(X2(:)-impool(:)) < 4*eps ))
            disp('Mean Pooling is fine !')
            X3 = L{3}.stepForward(X2);
            assert( all( all ( (X3 - NET.feedForward(im(:,:,1:8))) < 3.*eps )))
        end
        
        function testBackwardProp(testCase)
            %% test 4 : at least 90% prediction
            
            chem = [ fileparts(which('nn_tests.testConv')) filesep];
            load([chem 'ex4data1.mat']); y = transpose(y);
            X = reshape(transpose(X), [20,20,5000]);
            sz = size(X);
            % create network
            L{1} = nn.layerConv(sz(1:2),[8 8],25);

            L{2} = nn.layerFull( L{1}.nout ,80);
            L{3} = nn.layerFull( L{2}.nout ,10);
            NET = nn.network(L);
            
            
            % train network
            options = optimset('MaxIter', 50);
            [nn_params, cost] = nn.fmincg(  @(w) NET.trainFcn(w,X,y) , NET.W, options);
            % predict
            ypred = NET.predict(X);
            accuracy =  (1 - sum(y~=ypred) / length(y)) *100;
            disp(['Prediction accuracy : ' num2str(accuracy) '%' ])
            assert(accuracy > 90)
            
        end
    end
    
    
end


