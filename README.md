# README #

### What is this repository for? ###

* Neural Network framework in object oriented matlab
* 0.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Matlab search path to span folder and subfolders
* nn_tests package is the testing package, run is the file to run
* Full Connect layer is the only one fully implemented : nn_tests.testFullConnect shows a complete example of training on the MNIST dataset.