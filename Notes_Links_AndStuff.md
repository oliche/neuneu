# Provides some code and testing data for convnets
http://www.robots.ox.ac.uk/~vgg/practicals/cnn/

# Complete explanation
https://grzegorzgwardys.wordpress.com/2016/04/22/8/

# Stanford University classes
http://cs231n.github.io/convolutional-networks/
http://cs231n.github.io/optimization-2/
http://ufldl.stanford.edu/tutorial/supervised/ExerciseConvolutionalNeuralNetwork/